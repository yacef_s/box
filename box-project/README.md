# TIC-UNI2

Projet Box réalisé par YACEF Souleyman

**Consigne**

Vous devrez mettre en place une architecture Web utilisant des containers via Docker au moyen de docker-compose. Il vous faudra créer 3 containers.
L'architecture est composée des éléments suivants :

D'une base de données sous MySQL contenant une table Users (avec au minimum les champs ID, username, email et password)
D'un serveur PHP
D'un front (langage libre)
Les 3 containers devront être reliés entre eux : le back sera connecté à la BDD, et les informations devront être affichées sur le front.

**Setup**

Vous devez mettre en place un environnement permettant de déployer vos containers.
La rédaction de scripts d'installation / configuration est fortement encouragée, aussi bien pour vous en cas de besoin que dans l'appréciation générale du projet.


## Objectif 

Mis en place d'un gestionnaire d'environnements permettant d'effectuer :
- la création d'un environnement Debian dédié pour une application
- l'installation d'une application et de ses dépendances en ce basant sur APT
- l'exécution d'une application dans un environnement isolé

## Préparation

Pour la préparation du système de fichiers on va se basser dans le dossier **/var/lib/box/base/**.

On est parti d'une d'une archive qui contient le système de fichiers correspondant à une installation basique de Debian 10.

Dans le fichier **test.py**, j'ai directement téléchargé cette archive avec "urllib.request.urlretrieve" puis je l'ai décompressé avec la "tar" dans /var/lib/box/base/ pour faire la base.

## Exécution dans l'environnement

Pour cette parti on a recrée et monté quelque fichier manquant comme **/dev/random** avec la commande mknod :
- os.system('sudo mknod /var/lib/box/base/dev/random c 1 8')

et pour **/proc/** et **/sys** avec mount :
- sudo mount -tproc /proc /var/lib/box/base/proc/
- sudo mount --bind /sys /var/lib/box/base/sys/

Puis on a utilisé **os.chroot()** pour appliquer la fonction de chroot et changer de racine pour pouvoir faire os.system("echo 'nameserver 8.8.8.8' > /etc/resolv.conf") et donner l'accès a internet.

## Configuration d'un environnement

On a crée manuellement le fichier mongo.yml et on a essayé de faire le parsing du fichier mongo.yml pour pouvoir récupérer les information comme le nom ou la clé.

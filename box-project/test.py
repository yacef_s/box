import stat
import os
import sys
import shutil
import tarfile
import urllib.request

path2 = '/var/lib/box/base/'
path1 = '/var/lib/box/'
url = 'https://raw.githubusercontent.com/debuerreotype/docker-debian-artifacts/3503997cf522377bc4e4967c7f0fcbcb18c69fc8/buster/slim/rootfs.tar.xz'
file_name = 'rootfs.tar.xz'
proc = '/var/lib/box/base/proc'
system = '/var/lib/box/base/sys'
random = '/var/lib/box/base/dev/random/'

def arg():
 if sys.argv[1] == "build":
  if sys.argv[2] == "mongo.yml":
   file = open("mongo.yml", "r")
   lines = file.readlines()
   file.close

   for line in lines:
    print (line)

os.system('sudo apt install -y python3-pip')
os.system('pip3 install pyyaml')

import yaml

if os.path.exists(proc):
 os.system('sudo umount /var/lib/box/base/proc/')
 shutil.rmtree(proc)

if os.path.exists(system):
 os.system('sudo umount /var/lib/box/base/sys/')
 shutil.rmtree(system)

if os.path.exists(random):
 os.remove(random)

if os.path.exists(path1):
 shutil.rmtree(path1)
 os.mkdir(path1)
 os.mkdir(path2)
 print('base crée')
else:
 os.mkdir(path1)
 os.mkdir(path2)
 print ('base crée')

urllib.request.urlretrieve(url, file_name)
print ('Fichier téléchargé')
tar = tarfile.open(file_name, 'r:xz')
tar.extractall(path2)
tar.close()
print ('Fichier extrait')
os.remove(file_name)
print ('rootfs.tar.xz supprimer')

os.system('sudo mount -tproc /proc /var/lib/box/base/proc/')
print ('proc monté')

os.system('sudo mount --bind /sys /var/lib/box/base/sys/')
print ('sys monté')

os.system('sudo mknod /var/lib/box/base/dev/random c 1 8')
print ('/dev/random crée')

os.chroot(path2)
os.chdir('/')
os.system("echo 'nameserver 8.8.8.8' > /etc/resolv.conf")

